<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/reg','RegisterController@register')->name('register');
Route::get('/uploadFile','RegisterController@uploadFile')->name('uploadFile');
Route::post('/saveFile','RegisterController@saveFile')->name('saveFile');
Route::get('/list','RegisterController@getList')->name('getList');
