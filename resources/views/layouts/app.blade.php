<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @section('stylesheets')
        <link rel="stylesheet" href="{{ asset("css/app.css") }}">
    @show
    @section('javascripts')
        <script src="{{asset('js/app.js')}}"></script>
    @show
    <title>@yield('title')</title>
</head>
<body>
@section('navbar')
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
        <div class="container">
            <a class="navbar-brand" href=""><?php echo "Laravel"; ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                    aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

        </div>
    </nav>
@show
<div class="container-md">
    @yield('container')
</div>

@section('footer')
    <hr class="w-25 p-2" style="background-color: #eee;">
@show
</body>
</html>
