@extends('layouts.app')
@section('title','upload file')
@section('container')
    <div class="col-md-6 mx-auto">

        <form class="jumbotron jumbotron-flud text-center" action="{{ route('saveFile') }}"
              method="post" enctype="multipart/form-data">
            <input type="file" name="file">
            <button type="submit" class="btn btn-primary">Upload</button>
            @csrf
        </form>

    </div>
@endsection
