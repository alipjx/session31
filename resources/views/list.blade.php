@extends('layouts.app')
@section('title','list show')
@section('container')
    <table class="table table-bordered table-striped table-hover bg-yellow">
        <thead>
        <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Number</th>
        </tr>
        </thead>

        <tbody>
        @foreach($data as $item)
            <tr>
                <td>{{$item['name']}}</td>
                <td>{{$item['age']}}</td>
                <td>{{$item['number']}}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@endsection
