<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

class RegisterController
{
    public function register()
    {
        return view('reg');
    }

    public function uploadFile()
    {
        return view('upFile');
    }

    public function saveFile(Request $request)
    {

        $request->file('file')->store('photos');

        return back();
    }

    public function getList()
    {
        $data = [
            ["name" => "Ali", "age" => "25" , "number" => "09213465251"],
            ["name" => "Kiarash", "age" => "20" , "number" => "09124585251"],
            ["name" => "Sajjad", "age" => "22" , "number" => "09351654782"],
            ["name" => "Mohammad", "age" => "23" , "number" => "09302124257"],
        ];
        return view('list',['data' => $data]);
    }
}
